# OpenCV mutliple image stream server
Receives byte encoded images from network devices such as Android phones. It uses Boost Threads to receive multiple streams while listening on separate ports.

When pressing space, the program captures the recent images and stores them to disk. These can be used to do stereo calibration.
