#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include <vector>
#include <boost/thread/thread.hpp>

using boost::asio::ip::tcp;
using namespace std;
using namespace cv;

const int WIDTH = 480;
const int HEIGHT = 320;
const int CHANNELS = 1;
const int MAT_SIZE = WIDTH * HEIGHT * CHANNELS;

Mat img1 = Mat::zeros(HEIGHT, WIDTH, CV_8UC1);
Mat img2 = Mat::zeros(HEIGHT, WIDTH, CV_8UC1);
bool flag = false;  // if flag is false ,the thread is not ready to show the mat frame

bool rotateImg = false;

boost::mutex leftMtx, rightMtx;

void runTCPServer(Mat &img, int port, boost::mutex &mtx)
{

	try
	{
		boost::asio::io_service io_service;
		boost::array<char, MAT_SIZE> buf;
		tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), port));

		while (1)
		{
			tcp::socket socket(io_service);
			acceptor.accept(socket);

			boost::system::error_code error;
			size_t len = boost::asio::read(socket, boost::asio::buffer(buf, MAT_SIZE), error);

			//cout << "get data length :" << len << endl; /* disp the data size recieved */

			std::vector<uchar> vectordata(buf.begin(), buf.end());
			cv::Mat data_mat(vectordata, true);

			Mat newImg = data_mat.reshape(CHANNELS, HEIGHT);

			if (!rotateImg) {
				if (newImg.rows == HEIGHT && newImg.cols == WIDTH) {
					mtx.lock();
					img = newImg;
					mtx.unlock();
				}
			}
			else {
				Mat rotated;
				flip(newImg.t(), rotated, 0);

				if (rotated.cols == HEIGHT && rotated.rows == WIDTH) {
					mtx.lock();
					img = rotated;
					mtx.unlock();
				}
			}

			//cout << "reshape over" << endl;
			flag = true;
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

int main()
{
	boost::thread thrd1(&runTCPServer, boost::ref(img1), 3200, boost::ref(leftMtx));
	boost::thread thrd2(&runTCPServer, boost::ref(img2), 3201, boost::ref(rightMtx));

	int curFrame = 1;

	while (1)
	{
		if (flag)
		{
			leftMtx.lock();
			rightMtx.lock();

			Mat imgLeft = img1.clone();
			Mat imgRight = img2.clone();

			leftMtx.unlock();
			rightMtx.unlock();

			imshow("img1", imgLeft);
			imshow("img2", imgRight);

			int key = waitKey(10);
			if (key == ' ') {
				imwrite("../images/imageA" + std::to_string(curFrame) + ".jpg", imgLeft);
				imwrite("../images/imageB" + std::to_string(curFrame) + ".jpg", imgRight);

				cout << "Saved " << curFrame << endl;

				curFrame++;
			}
			else if (key == 27) break; // stop capturing by pressing ESC 
		}
	}

	thrd1.join();
	thrd2.join();

	return 0;
}
